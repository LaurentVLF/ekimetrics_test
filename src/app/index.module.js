/* global moment:false */

import { config } from './index.config';
import { routerConfig } from './index.route';
import { runBlock } from './index.run';
import { DashboardController } from './dashboard/dashboard.controller';
import { ProductHuntService } from '../app/services/productHunt.service';

angular.module('ekimetric', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria', 'ui.router', 'ui.bootstrap', 'toastr', 'nvd3', 'blockUI'])
    .constant('moment', moment)
    .config(config)
    .config(routerConfig)
    .run(runBlock)
    .service('productHuntService', ProductHuntService)
    .controller('DashboardController', DashboardController);