export class ProductHuntService {
    constructor($log, $http, toastr) {
        'ngInject';

        this.$log = $log;
        this.$http = $http;
        this.toastr = toastr;
        this.apiHost = 'https://api.producthunt.com';
        this.initHttp()
    }

    /**
     * Configuration du header (pourrait être deporté dans un fichier de config type properties)
     */
    initHttp() {
        this.headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer 03960afd67985a8bd126867f6813e4c3ba54186f8126b1207c940fd45e1403ff',
            'Host': 'api.producthunt.com'
        }
    }

    /**
     * Appel serveur Get pour récupérer les détail d'un post par son id
     * @param {number} id 
     */
    getPostDetail(id) {
        return this.$http({ method: 'GET', url: this.apiHost + '/v1/posts/' + id, headers: this.headers })
            .then((response) => {
                return response.data;
            })
            .catch((error) => {
                this.toastr.error('GET post details Failed', error);
            });
    }

    /**
     * Appel serveur Get pour récupérer tous les posts d'un jour par "days_ago"
     * @param {number} nbrJours 
     */
    getAllPostToday(nbrJours) {
        return this.$http({ method: 'GET', url: this.apiHost + '/v1/posts?days_ago=' + nbrJours, headers: this.headers })
            .then((response) => {
                return response.data;
            })
            .catch((error) => {
                this.toastr.error('GET posts Failed.', error);
            });
    }

    /**
     * Appel serveur Get pour récupérer les posts d'un jour précis par sa date
     * @param {string} date 
     */
    getPostByDate(date) {
        return this.$http({ method: 'GET', url: this.apiHost + '/v1/posts?day=' + date, headers: this.headers })
            .then((response) => {
                return response.data;
            })
            .catch((error) => {
                this.toastr.error('GET posts by date Failed.', error);
            });
    }
}