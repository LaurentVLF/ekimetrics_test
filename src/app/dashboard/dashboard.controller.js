export class DashboardController {
    constructor(productHuntService, $log, moment, blockUI, toastr) {
        'ngInject';
        this.service = productHuntService;
        this.log = $log;
        this.moment = moment;
        this.blockUI = blockUI;
        this.toastr = toastr;

        this.nbrJours = 7;
        this.firstTime = true;
        this.detail = undefined;

        // Configuration datepicker
        this.dt = new Date();
        this.popup = {
            opened: false
        };
        this.format = 'dd-MM-yyyy';
        this.dateOptions = {
            maxDate: new Date(),
            startingDay: 1
        };

        // Datas graph
        this.data = []

        // Premier appel serveur pour initialisation page.
        this.init()
    }

    /**
     * Datepicker.
     */
    open() {
        this.popup.opened = true;
    }

    /**
     * Init
     */
    init() {
        this.initGraph()
        this.getPostsByDate()
    }


    /**
     * Initialisation du graph.
     */
    initGraph() {
        this.options = {
            chart: {
                type: 'pieChart',
                height: 800,
                x: function(d) { return d.key; },
                y: function(d) { return d.y; },
                showLabels: true,
                duration: 500,
                labelThreshold: 0.01,
                labelSunbeamLayout: true,
                legend: {
                    margin: {
                        top: 5,
                        right: 35,
                        bottom: 5,
                        left: 0
                    }
                },
                callback: (chart) => {
                    chart.pie.dispatch.on('elementClick', (e) => {
                        this.getPostDetail(e.data);
                    });
                }
            }
        }
    }

    /**
     * Méthode de récupération des post en fonction de la date selectionnée.
     */
    getPostsByDate() {
        this.data = [];
        this.detail = undefined;
        this.blockUI.start();
        this.service.getPostByDate(this.formatDate(this.dt)).then(res => {
            this.log.info('Qu\'est ce qu\'on a à la date du ' + this.formatDate(this.dt) + ' ?', res)
            this.resultats = res;
            this.formatResultForGraph();
        }).catch(err => this.toastr.error(err)).finally(() => this.blockUI.stop())
    }

    /**
     * Méthode permettant de récupérer les details d'un post.
     * @param {*} data 
     */
    getPostDetail(data) {
        this.service.getPostDetail(data.id).then(detail => {
            this.detail = detail;
        }).catch(err => this.toastr.error(err))
    }

    /**
     * Méthode permettant de passer la date au bon format pour l'appel serveur.
     * @param {*} date 
     */
    formatDate(d) {

        let month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        let bonneDate = [year, month, day].join('-');
        this.log.info(bonneDate);
        return bonneDate;
    }

    /**
     * Méthode permettant de formater les datas pour affichage sur le graph
     */
    formatResultForGraph() {
        if (this.resultats && this.resultats.posts) {
            for (let post of this.resultats.posts) {
                let data = {
                    key: post.name,
                    y: post.comments_count,
                    id: post.id
                }
                this.data.push(data);
            }
        }
    }
}